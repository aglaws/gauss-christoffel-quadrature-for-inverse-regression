function [E, W, C_IR] = lanczos_SIR(X, f, w, iter)

% Determine the number of paperameters m
m = size(X, 2);

% Get Lanczos vectors
[~, V] = dlanczos(f, sqrt(w), 'iter', iter);

% Compute C_IR matrix
B = V'*bsxfun(@times, X, sqrt(w));
C_IR = B'*B;

% LSIR components
[E, W] = get_eigs(C_IR);

end

