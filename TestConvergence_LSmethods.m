clear variables
close all
clc

addpath 'pmpack'

% Set SDR method (SIR/LSIR = 1; SAVE/LSAVE = 2)
sdr_method = 1;

% Chose test function (Example 2 = 2; Example 3 = 3)
func_flag = 3;

if func_flag == 2
    % Set dimension of problem
    m = 5;

    % Set Lanczos-Stieltjes params
    M_per_True = 21; iters_True = 35;
    M_per = 3:2:21; iters = 5:5:35;

    % Initialize quadrature rule
    rho = 'Gaussian';
    
elseif (func_flag == 3)
    % Set dimension of problem --> m
    m = 6;

    % Set Lanczos-Stieltjes params
%     M_per_True = 17; iters_True = 35;
%     M_per = 3:2:17; iters = 5:5:35;
    M_per_True = 11; iters_True = 20;
    M_per = 3:2:11; iters = 5:5:20;

    % Initialize quadrature rule
    rho = 'Legendre';
end

% Get quadrature nodes & weights for 'true' case
s = [];
for i=1:m
    s = [s; parameter(rho)];
end
[X, nu] = gaussian_quadrature(s, M_per_True*ones(1, m));

% Evaluate f at quadrature nodes
f = test_func(X, func_flag);

% Perform Lanczos-Stieltjes SDR method
if sdr_method == 1
    [~, ~, C_True] = lanczos_SIR(X, f, nu, iters_True);
else
    [~, ~, C_True] = lanczos_SAVE(X, f, nu, iters_True);
end

fprintf(['Done constructing ''True'' case for function ' num2str(func_flag) '\n']);

abs_err_2d = zeros(length(M_per)-1, length(iters)-1);
rel_err_Mper = zeros(length(M_per)-1, 1); C0_Mper = zeros(m);
rel_err_iters = zeros(length(iters)-1, 1); C0_iters = zeros(m);
for i = 1:length(M_per)
    
    % Get quadrature nodes & weights and evaluate function
    [X, nu] = gaussian_quadrature(s, M_per(i)*ones(1, m));
    f = test_func(X, func_flag);
    
    for j = 1:length(iters)
        
        % Perform Lanczos-Stieltjes SDR method
        if sdr_method == 1
            [~, ~, C] = lanczos_SIR(X, f, nu, iters(j));
        else
            [~, ~, C] = lanczos_SAVE(X, f, nu, iters(j));
        end
        
        % Computer relative matrix errors
        if (i < length(M_per)) && (j < length(iters))
            abs_err_2d(i, j) = norm(C_True - C, 'fro')/norm(C_True, 'fro');
        end
        
        % Compute subsequent matrix differences for increasing Lanczos
        % iterations
        if (i == length(M_per)) && (j < length(iters))
            rel_err_iters(j) = norm(C0_iters - C, 'fro')/norm(C, 'fro');
            
            C0_iters = C;
        end
        
        % Compute subsequent matrix differences for increasing quadrature 
        % nodes        
        if (j == length(iters)) && (i < length(M_per))
            rel_err_Mper(i) = norm(C0_Mper - C, 'fro')/norm(C, 'fro');
            
            C0_Mper = C;
        end

        fprintf(['Done with M_per = ' num2str(M_per(i)) ' and iters = ' num2str(iters(j)) '\n']);
    end
end
abs_err_2d(abs_err_2d == 0) = min(min(abs_err_2d(abs_err_2d ~= 0)));

clear X f nu s

% Plot subsequent differences for increasing numbers of quadrature nodes
figure('Position', [0, 0, 600, 400])
loglog(M_per(1:end-1).^m, rel_err_Mper, '-o', ...
       'LineWidth', 2, 'Color', 'k', ...
       'MarkerEdgeColor', 'k', 'MarkerFaceColor', 'k');
xlabel('Number of Quadrature Nodes', 'FontSize', 16)
ylabel('Matrix Difference', 'FontSize', 16)
set(gca, 'FontSize', 16, ...
         'YLim', [min(rel_err_Mper), max(rel_err_Mper)], 'YTick', 10.^(-10:2:0),...
         'XLim', [M_per(1).^m, M_per(end-1).^m], 'XTick', 10.^(3:7))

% Plot subsequent differences for increasing numbers of Lanczos iterations
figure('Position', [0, 0, 600, 400])
semilogy(iters(1:end-1), rel_err_iters, '-o', ...
         'LineWidth', 2, 'Color', 'k', ...
         'MarkerEdgeColor', 'k', 'MarkerFaceColor', 'k');
xlabel('Number of Lanczos Iterations', 'FontSize', 16)
ylabel('Matrix Difference', 'FontSize', 16)
set(gca, 'FontSize', 16, ...
         'YLim', [min(rel_err_iters), max(rel_err_iters)], 'YTick', 10.^(-10:2:0),...
         'XLim', [iters(1), iters(end-1)], 'XTick', iters(1):5:iters(end-1))

% Plot relative matrix errors
figure('Position', [0, 0, 600, 400])
contours = log(10.^(floor(log10(min(min(abs_err_2d)))):...
                    ceil(log10(max(max(abs_err_2d))))));
contourf(iters(1:end-1), M_per(1:end-1).^m, log(abs_err_2d), contours)
set(gca, 'YDir', 'Reverse', 'YScale', 'Log', ...
    'XLim', [iters(1), iters(end-1)], 'XTick', iters(1):5:iters(end-1),...
    'YLim', [M_per(1)^m, M_per(end-1)^m], 'YTick', 10.^(2:8))
xlabel('Number of Lanczos Iterations', 'FontSize', 16)
ylabel('Number of Quadrature Nodes', 'FontSize', 16)
set(gca, 'FontSize', 16)
hCB = colorbar(); 
cb_Ticks = exp(get(hCB, 'YLim'));
cb_lb = floor(log10(cb_Ticks(1))); cb_ub = ceil(log10(cb_Ticks(2)));
cb_Ticks = 10.^(cb_lb-1:1:cb_ub);
set(hCB, 'YTick', log(cb_Ticks), 'YTickLabel', cb_Ticks, 'FontSize', 16);
caxis([log(cb_Ticks(2)), log(cb_Ticks(end-1))])
view(2)