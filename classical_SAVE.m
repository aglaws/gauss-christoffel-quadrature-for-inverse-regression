function [E, W, C_SAVE] = classical_SAVE(X, f, H)

% Determine the number of samples M and number of paperameters m
[M, m] = size(X);

% Bin data according to responses
bins = prctile(f, linspace(0, 100, H+1));
bins(1) = bins(1) - eps;

% Compute C_SAVE matrix
C_SAVE = zeros(m);
for i = 1:H
    in_slice = (f > bins(i)) & (f <= bins(i+1));
    n_h = sum(in_slice);
    if (n_h ~= 0)
        X_tilde = bsxfun(@minus, X(in_slice, :), mean(X(in_slice, :)));
        sweights = ones(n_h, 1)/n_h;
        if (n_h > 1)
            V = eye(m) - (X_tilde'*bsxfun(@times, X_tilde, sweights))/(1 - sum(sweights.^2));
        else
            V = eye(m);
        end
        C_SAVE = C_SAVE + (n_h/M)*V*V;
    end
end

% Get SAVE components
[E, W] = get_eigs(C_SAVE);

end