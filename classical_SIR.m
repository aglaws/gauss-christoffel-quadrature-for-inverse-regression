function [E, W, C_SIR] = classical_SIR(X, f, H)

% Determine the number of samples M and number of paperameters m
[M, m] = size(X);

% Bin data according to responses
bins = prctile(f, linspace(0, 100, H+1));
bins(1) = bins(1) - eps;

% Compute C_SIR matrix
C_SIR = zeros(m);
for i = 1:H
    in_slice = (f > bins(i)) & (f <= bins(i+1));
    if any(in_slice)
        p_hat = sum(in_slice)/M;
        m_hat = mean(X(in_slice, :))';
        
        C_SIR = C_SIR + p_hat*(m_hat*m_hat');
    end
end

% Get SIR components
[E, W] = get_eigs(C_SIR);

end