function V = otlcircuit(X)

% Uniform input distribution
xl = [50 25 0.5 1.2 0.25 50];
xu = [150 70 3 2.5 1.2 300];

% shift and scale
XX = bsxfun(@plus, bsxfun(@times, 0.5*(X + 1), xu - xl), xl);

Rb1 = XX(:,1); Rb2 = XX(:,2); Rf = XX(:,3); 
Rc1 = XX(:,4); Rc2 = XX(:,5); beta = XX(:,6);

T1 = (( 0.74 + (12*Rb2 ./ (Rb1 + Rb2)) ).*beta.*( Rc2 + 9 )) ./ ...
    ( beta .* (Rc2 + 9) + Rf );

T2 = (11.35*Rf) ./ ( beta.*(Rc2 + 9) + Rf );

T3 = ( 0.74 .* Rf .* beta .* (Rc2 + 9) ) ./ ...
    ( Rc1 .* ( beta .* (Rc2 + 9) + Rf ) );

V = T1 + T2 + T3;