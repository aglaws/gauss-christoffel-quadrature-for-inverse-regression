function test_main_pdetoolbox
% Tests that require the pdetoolbox

addpath('../demo');
try
    
    %% examples_test:galerkin_preconditioner
    P = pmpack_problem('elliptic');
    s0 = midpoint(P.s);
    R = chol(P.A(s0));
    P1 = @(x) galerkin_preconditioner(R',x);
    P2 = @(x) galerkin_preconditioner(R,x);
    X1 = spectral_galerkin(P.A,P.b,P.s,2,...
          'solver',@(A,b) minres(A,b,1e-5,100,P1,P2));
    X2 = spectral_galerkin(P.A,P.b,P.s,2,... % no preconditioning
          'solver',@(A,b) minres(A,b,1e-5,1500));
    %% elliptic2d
	P = elliptic_func('trunc',2); errs = zeros(19,1);
    for n=2:20 % try pseudospectral with 2 to 10 basis functions
      X = pseudospectral(P.solve,P.s,n);
      errs(n-1) = P.N*error_estimate('MinCoeff',X)/norm(P.b(0));
    end
    semilogy(2:20,errs,'.-');
    
    %% elliptic
    P = elliptic_func();
    [X,r] = spectral_galerkin(P.A,P.b,P.s,2);
    pdesurf(P.mesh.p,P.mesh.t,X.coefficients(:,1)); view(2); colorbar;
    
    rmpath('../demo');
catch me
    rmpath('../demo');
    rethrow(me);
end
