function f = test_func(X, ind)

m = size(X, 2);

rng(42)
U = orth(randn(m));

switch ind
    case 1 
        % Quadratic function
        s = [10.^(0:-1:-m+2), 0];
        Sigma = diag(s);
        
        H = U*Sigma*U';
        g = U(:, m);
        
        f = X*g + sum((X*H).*X, 2);
    case 2
        % 1d trig ridge function
        a = U(:, 1);
        
        f = (X*a).*cos((X*a)/(2*pi));
    case 3
        % otlcircuit function
        f = otlcircuit(X);
end

end

