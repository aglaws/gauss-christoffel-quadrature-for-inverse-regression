clear variables
close all
clc

addpath 'pmpack'

% Set SDR method (SIR/LSIR = 1; SAVE/LSAVE = 2)
sdr_method = 2;

% Chose test function (Example 2 = 2; Example 3 = 3)
func_flag = 3;

if func_flag == 2
    % Set dimension of problem
    m = 5;

    % Set Lanczos-Stieltjes params
    M_GQ_True = 21; iters_True = 35;
    M_MC = round(logspace(2, 6, 10)); lanc_iters = 5:5:30;
    
    % Initialize quadrature rule
    rho = 'Gaussian';
    
elseif (func_flag == 3)
    % Set dimension of problem --> m
    m = 6;

    % Set Lanczos-Stieltjes params
    M_GQ_True = 17; iters_True = 35;
    M_MC = round(logspace(2, 7, 10)); lanc_iters = 5:5:30;

    % Initialize quadrature rule
    rho = 'Legendre';
end

% Set number slices for SIR/SAVE algorithm
R_list = [5, 25, 50, 100, 500, 1000];

% Set number of bootstrap replicates to compute
n_boot = 10;

% Get quadrature nodes & weights
s = [];
for i=1:m
    s = [s; parameter(rho)];
end
[X, nu] = gaussian_quadrature(s, M_GQ_True*ones(1, m));

% Evaluate function at quadrature nodes
f = test_func(X, func_flag);

% Perform Lanczos-Stieltjes SDR method for 'true' case
if sdr_method == 1
    [~, ~, C_True] = lanczos_SIR(X, f, nu, iters_True);
else
    [~, ~, C_True] = lanczos_SAVE(X, f, nu, iters_True);
end

clear X f nu
fprintf('Done with Lanczos-Stieltjes case. \n');

% Perform Monte Carlo sample study
err_slicing = zeros(length(R_list), length(M_MC));
err_lanczos_MC = zeros(length(lanc_iters), length(M_MC));
for i = 1:length(M_MC)
    
    temp_err_slicing = zeros(length(R_list), n_boot);
    temp_err_lanczos_MC = zeros(length(lanc_iters), n_boot);
    for j = 1:n_boot
        
        % Get Monte Carlo samples
        if (func_flag == 2)
            X = randn(M_MC(i), m);
        elseif (func_flag == 3)
            X = 2*rand(M_MC(i), m) - 1;
        end
        
        % Evaluate f at MC samples
        f = test_func(X, func_flag);
        
        for r = 1:length(R_list)
            
            % Perform slice-based SDR method
            if sdr_method == 1
                [~, ~, C] = classical_SIR(X, f, R_list(r));
            else
                [~, ~, C] = classical_SAVE(X, f, R_list(r));
            end
            
            % Compute relative matrix error
            temp_err_slicing(r, j) = norm(C - C_True, 'fro')/norm(C_True, 'fro');
        end
        
        nu = ones(M_MC(i), 1)/M_MC(i);
        for k = 1:length(lanc_iters)
            
            % Perform Lanczos-Stieltjes SDR method
            if sdr_method == 1
                [~, ~, C] = lanczos_SIR(X, f, nu, lanc_iters(k));
            else
                [~, ~, C] = lanczos_SAVE(X, f, nu, lanc_iters(k));
            end
            
            % Compute relative matrix error
            temp_err_lanczos_MC(k, j) = norm(C - C_True, 'fro')/norm(C_True, 'fro');
        end
    end
    err_slicing(:, i) = mean(temp_err_slicing, 2);
    err_lanczos_MC(:, i) = mean(temp_err_lanczos_MC, 2);
    
    fprintf(['Done with Monte Carlo algorithms for ' num2str(M_MC(i)) ' samples. \n'])
end

clear X f nu

% Plot results
colors = [1, 0, 0; 0, 0, 1; 0, 0.5, 0; 0.5, 0, 1; 1, 0.5, 0; 0.8, 0.8, 0];
min_err = 10^floor(log10(min(min([err_slicing; err_lanczos_MC]))));
max_err = 10^ceil(log10(max(max([err_slicing; err_lanczos_MC]))));

% Plot MC slicing results
figure('Position', [0 0 600 450])
for r = 1:length(R_list)
    loglog(M_MC, err_slicing(r, :), ...
           'Marker', 'o', 'MarkerFaceColor', colors(r, :), ...
           'LineWidth', 2, 'Color', colors(r, :))
    hold on
end
if sdr_method == 1
   title('SIR with Monte Carlo samples', 'FontSize', 16)
elseif sdr_method == 2
   title('SAVE with Monte Carlo samples', 'FontSize', 16) 
end
xlabel('Number of Samples', 'FontSize', 16)
ylabel('Error', 'FontSize', 16)
set(gca, 'FontSize', 16, ...
    'XLim', [min(M_MC), max(M_MC)], 'XTick', 10.^(3:6), ...
    'YLim', [min_err, max_err], 'YTick', 10.^(-6:0))
legend(['R = ' num2str(R_list(1))], ...
       ['R = ' num2str(R_list(2))], ...
       ['R = ' num2str(R_list(3))], ...
       ['R = ' num2str(R_list(4))], ...
       ['R = ' num2str(R_list(5))], ...
       ['R = ' num2str(R_list(6))], ...
        'Location', 'SouthWest')
   

% Plot MC Lanczos results
figure('Position', [0 0 600 450])
for k = 1:length(lanc_iters)
    loglog(M_MC, err_lanczos_MC(k, :), ...
           'Marker', 'o', 'MarkerFaceColor', colors(k, :), ...
           'LineWidth', 2, 'Color', colors(k, :))
    hold on
end
if sdr_method == 1
   title('LSIR with Monte Carlo samples', 'FontSize', 16)
elseif sdr_method == 2
   title('LSAVE with Monte Carlo samples', 'FontSize', 16) 
end
xlabel('Number of Samples', 'FontSize', 16)
ylabel('Error', 'FontSize', 16)
set(gca, 'FontSize', 16, ...
    'XLim', [min(M_MC), max(M_MC)], 'XTick', 10.^(3:6), ...
    'YLim', [min_err, max_err], 'YTick', 10.^(-6:0))
legend(['k = ' num2str(lanc_iters(1))], ...
       ['k = ' num2str(lanc_iters(2))], ...
       ['k = ' num2str(lanc_iters(3))], ...
       ['k = ' num2str(lanc_iters(4))], ...
       ['k = ' num2str(lanc_iters(5))], ...
       ['k = ' num2str(lanc_iters(6))], ...
        'Location', 'SouthWest')