function [E, W] = get_eigs(C)

m = size(C, 1);

[W, E] = eig(C);

E = abs(E);
[E, ind] = sort(diag(E), 'descend');
W = W(:, ind);
mult = sign(W(1, :)); mult(mult == 0) = 1;
W = W.*repmat(mult, m, 1);

end