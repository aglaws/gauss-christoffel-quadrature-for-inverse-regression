clear variables
close all
clc

m = 3; iters = 5; func_flag = 1;

% Construct case 1
[X, nu] = multivar_CCQ(1, -1, 1, m);
f = test_func(X, func_flag);
X0 = X;

[ab, V] = dlanczos(f, sqrt(nu), 'iter', iters);
phi_f_0 = bsxfun(@rdivide, V, sqrt(nu));

[Q, lambda, omega] = gjacobi(ab);
Q0 = Q; lambda0 = lambda; omega0 = omega;

level = 2:7;
err_phi_f = zeros(length(level), iters);
err_lambda = zeros(length(level), iters);
err_omega = zeros(length(level), iters);
for i = 1:length(level)
    [X, nu, ind] = multivar_CCQ(level(i), -1, 1, m);
    f = test_func(X, func_flag);
    
    [ab, V] = dlanczos(f, sqrt(nu), 'iter', iters);
    phi_f = bsxfun(@rdivide, V, sqrt(nu));
    
    [Q, lambda, omega] = gjacobi(ab);
    
    % Absolute Errors
    err_phi_f(i, :) = max(abs(phi_f(ind, :) - phi_f_0));
    err_lambda(i, :) = abs(lambda - lambda0)';
    err_omega(i, :) = abs(omega - omega0)';
    
    phi_f_0 = phi_f; ab0 = ab; lambda0 = lambda; omega0 = omega;
    
    fprintf(['Level ' num2str(level(i)) ' completed. \n'])
end

% Plot errors in Lanczos vectors
figure('Position', [0, 0, 662, 390])
legend_list = {};
colors = [  1,   0,   0; ...
            0,   0,   1; ...
            0, 0.5,   0; ...
          0.5,   0,   1; ...
            1, 0.5,   0];
for i = 1:iters
    loglog((2.^level + 1).^m, err_phi_f(:, i), '-o', ...
             'LineWidth', 2, 'Color', colors(i, :), ...
             'MarkerEdgeColor', colors(i, :), 'MarkerFaceColor', colors(i, :))
    hold on
    
    legend_list{i} = ['\phi_{' num2str(i-1) '}'];
end
set(gca, 'XLim', [100, max(2.^level + 1).^m + 3e5], 'YLim', [1e-15, max(max(err_phi_f))],...
         'XTick', 10.^(2:6), 'FontSize', 16)
xlabel('Number of Clenshaw-Curtis Nodes', 'FontSize', 16)
ylabel('Maximum Polynomial Difference', 'FontSize', 16)
legend(legend_list, 'Location', 'EastOutside', 'FontSize', 16)

% Plot errors in lambda
figure('Position', [0, 0, 662, 390])
legend_list = {};
colors = [1, 0, 0; 0, 0, 1; 0, 0.5, 0; 0.5, 0, 1; 1, 0.5, 0];
for i = 1:iters
    loglog((2.^level + 1).^m, err_lambda(:, i), '-o', ...
             'LineWidth', 2, 'Color', colors(i, :), ...
             'MarkerEdgeColor', colors(i, :), 'MarkerFaceColor', colors(i, :))
    hold on
    
    legend_list{i} = ['\lambda_{' num2str(i-1) '}'];
end
set(gca, 'XLim', [100, max(2.^level + 1).^m + 3e5], 'YLim', [min(min(err_lambda)), 1], ...
         'XTick', 10.^(2:6), 'FontSize', 16)
title('Gauss-Christoffel Node Differences', 'FontSize', 16)
xlabel('Number of Clenshaw-Curtis Nodes', 'FontSize', 16)
ylabel('Difference', 'FontSize', 16)
legend(legend_list, 'Location', 'EastOutside', 'FontSize', 16)

% Plot errors in omega
figure('Position', [0, 0, 662, 390])
legend_list = {};
colors = [1, 0, 0; 0, 0, 1; 0, 0.5, 0; 0.5, 0, 1; 1, 0.5, 0];
for i = 1:iters
    loglog((2.^level + 1).^m, err_omega(:, i), '-o', ...
             'LineWidth', 2, 'Color', colors(i, :), ...
             'MarkerEdgeColor', colors(i, :), 'MarkerFaceColor', colors(i, :))
    hold on
    
    legend_list{i} = ['\omega_{' num2str(i-1) '}'];
end
set(gca, 'XLim', [100, max(2.^level + 1).^m + 3e5], 'YLim', [min(min(err_omega)), 1], ...
         'XTick', 10.^(2:6), 'FontSize', 16)
title('Gauss-Christoffel Weight Differences', 'FontSize', 16)
xlabel('Number of Clenshaw-Curtis Nodes', 'FontSize', 16)
ylabel('Difference', 'FontSize', 16)
legend(legend_list, 'Location', 'EastOutside', 'FontSize', 16)