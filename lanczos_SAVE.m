function [E, W, C_AVE] = lanczos_SAVE(X, f, w, iter)

% Determine the number of paperameters m
m = size(X, 2);

% Get Lanczos vectors
[~, V] = dlanczos(f, sqrt(w), 'iter', iter);

% Compute C_AVE matrix
mu_f_xi = bsxfun(@rdivide, V, sqrt(w))*(V'*bsxfun(@times, X, sqrt(w)));
centered_X = X - mu_f_xi;

C_AVE = eye(m);
for i = 1:iter
    C0 = centered_X'*bsxfun(@times, centered_X, sqrt(w).*V(:, i));
    if i == 1
        C_AVE = C_AVE - 2*C0;
    end
    
    C_AVE = C_AVE + C0^2;
end

% Get LSAVE components
[E, W] = get_eigs(C_AVE);

end