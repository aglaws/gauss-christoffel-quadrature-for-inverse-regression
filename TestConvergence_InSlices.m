clear variables
close all
clc

addpath 'pmpack'

% Set SDR method (SIR/LSIR = 1; SAVE/LSAVE = 2)
sdr_method = 1;

% Chose test function (Example 2 = 2; Example 3 = 3)
func_flag = 3;

if func_flag == 2
    % Set dimension of problem
    m = 5;

    % Set Lanczos-Stieltjes params
    M_GQ = 21; iters = 35;

    % Initialize quadrature rule
    rho = 'Gaussian';
    
elseif (func_flag == 3)
    % Set dimension of problem --> m
    m = 6;

    % Set Lanczos-Stieltjes params
    M_GQ = 17; iters = 35;
    
    % Initialize quadrature rule
    rho = 'Legendre';
end

% Get quadrature nodes & weights
s = [];
for i=1:m
    s = [s; parameter(rho)];
end
[X, nu] = gaussian_quadrature(s, M_GQ*ones(1, m));

% Evaluate function at quadrature nodes
f = test_func(X, func_flag);

% Perform Lanczos-Stieltjes SDR method
if sdr_method == 1
    [~, ~, C_True] = lanczos_SIR(X, f, nu, iters);
else
    [~, ~, C_True] = lanczos_SAVE(X, f, nu, iters);
end

clear X f nu s
fprintf('Done with Lanczos-Stieltjes case. \n');

% Set number of Monte Carlo samples for slice-based methods
M = 1e8;

% Set number slices for SIR/SAVE algorithm
R_list = [5, 10, 25, 50, 100, 250, 500, 1000];

% Set number of bootstrap replicates to compute
n_boot = 10;

mat_err = zeros(length(R_list), 1);
for r = 1:length(R_list)
    R = R_list(r);
    
    temp_mat_err = zeros(n_boot, 1);
    for i = 1:n_boot
        
        % Get Monte Carlo samples
        if (func_flag == 2)
            X = randn(M, m);
        elseif (func_flag == 3)
            X = 2*rand(M, m) - 1;
        end
        
        % Evaluate f at MC samples
        f = test_func(X, func_flag);
        
        % Perform slice-based SDR method
        if sdr_method == 1
            [~, ~, C] = classical_SIR(X, f, R);
        else
            [~, ~, C] = classical_SAVE(X, f, R);
        end
        
        % Compute relative matrix error
        temp_mat_err(i) = norm(C_True - C, 'fro')/norm(C_True, 'fro');

        fprintf(['Done with traditional case with R = ' num2str(R) ' slices. \n']);
    end
    mat_err(r) = mean(temp_mat_err);
    
end

clear X f

% Plot results
figure('Position', [0, 0, 600, 400])
loglog(R_list, mat_err, 'o-', ...
         'LineWidth', 2, 'MarkerSize', 10, ...
         'MarkerFaceColor', 'k', 'MarkerEdgeColor', 'k', ...
         'Color', 'k')
hold on
xlabel('Number of Slices', 'FontSize', 16);
ylabel('Matrix Error', 'FontSize', 16);
set(gca, ...
    'XLim', [min(R_list)-0.1, max(R_list)], ...
    'FontSize', 16)