Scripts for generating figures for the paper

'Gauss-Christoffel quadrature for inverse regression: an application to computer experiments' A. Glaws and P. G. Constantine

https://doi.org/10.1007/s11222-018-9816-4