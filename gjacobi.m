function [Q,p,w] = gjacobi(ab)
%GJACOBI Compute Gauss rule and Fourier transform
%
% [Q,p,w] = gjacobi(ab)
%
% INPUTS
%    ab    Nx2 array of recurrence coefficients
%
% OUTPUTS
%    Q     Orthogonal Fourier transform, Q=PW
%    p     Gauss points
%    w     Gauss weights

J=full(gallery('tridiag',...
    sqrt(ab(2:end,2)),ab(1:end,1),sqrt(ab(2:end,2))));

[Q,D]=eig(J);
[p,I]=sort(diag(D));
w=sqrt(ab(1,2))*Q(1,I)'.^2;
Q=Q(:,I);
for i=1:length(w), Q(:,i)=sign(Q(1,i))*Q(:,i); end