function [ab,V] = dlanczos(a,v,varargin)
%DLANCZOS Lanczos on a diagonal matrix.
%
% [ab,V] = dlanczos(a,v)
% [ab,V] = dlanczos(a,v,'iter',n,'tol',tol)
%
% This is a naive implementation of Lanczos. Algorithm 1.1 (Paige) from
% Meurant (Lanczos and CG). If 'iter' is not specified, iterations run 
% until orthogonality is lost, which implies measure has converged.
% 
% INPUTS
%    a     Diagonal elements of a matrix.
%    v     Starting vector.
%
% OPTIONAL INPUTS
%    iter  Number of iterations. Useful for saving memory.
%    tol   Tolerance (-14).
%
% OUTPUTS
%    ab    Nx2 array of recurrence coefficients.
%    V     Lanczos vectors.

if nargin<2, error('Not enough input arguments.'); end

% default values
iter=0;
tol=-14; % tolerance for orthogonality

for i=1:2:length(varargin)-1
    switch lower(varargin{i})
        case 'iter'
            iter=varargin{i+1};
        case 'tol'
            tol=varargin{i+1};
        otherwise
            error('Unrecognized option: %s\n',varargin{i});
    end
end

if iter, N=iter; else N=length(a); end

% allocate memory
ab=zeros(N,2);
V=zeros(length(v),N);

% initialize
ab(1,2)=norm(v)^2;
v0=v./sqrt(ab(1,2)); V(:,1)=v0;
alpha=v0'*(a.*v0); ab(1,1)=alpha;
vstar=(a-alpha).*v0; 

for i=2:N
    eta=norm(vstar); 
    v=vstar./eta;  
    
    % Check orthogonality
    ortho=log10(norm(eye(i-1)-(V(:,1:i-1)'*V(:,1:i-1)),'fro'));
    % ortho=log10(abs(v0'*v)); % cheaper, but less reliable.
    if ~iter, if ortho>tol, i=i-2; break; end; end
    
    ab(i,2)=eta^2; V(:,i)=v;
    u=(a.*v)-eta*V(:,i-1);
    alpha=v'*u; ab(i,1)=alpha;
    vstar=u-alpha*v;
end
%fprintf('Lanczos stopped after %d iterations. Log10(Abs(Ortho))=%4.2f\n',i,ortho);

V=V(:,1:i); ab=ab(1:i,:);
