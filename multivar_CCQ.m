function [X, w, ind] = multivar_CCQ(level, a, b, m)

N = 2^level + 1;

[X0, w0] = fclencurt(N, a, b);
ind0 = uint8(rem(1:N, 2))';

X = 1; w = 1; ind = uint8(1);
for i = 1:m
    X = [kron(X, ones(N, 1)), kron(ones(size(X, 1), 1), X0)];
    ind = [kron(ind, uint8(ones(N, 1))), kron(uint8(ones(size(ind, 1), 1)), ind0)];
    w = kron(w, w0);
end
X = X(:, 2:end);
ind = all(ind, 2);

end